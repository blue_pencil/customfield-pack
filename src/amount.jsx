import ForgeUI, { CustomField, CustomFieldView, Text, render, useProductContext, Fragment } from "@forge/ui";

const View = () => {
    const { platformContext: { fieldValue } } = useProductContext();
    let amount = null;
    if (fieldValue !== undefined && fieldValue !== null) {
        amount = "$ " + fieldValue.toFixed(2);
    }
    return (
        <CustomFieldView>
            <Fragment>
                <Text>{amount}</Text>
            </Fragment>
        </CustomFieldView>
    );
};

export const run = render(
    <CustomField
        view={<View/>}
    />
);
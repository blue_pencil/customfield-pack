import ForgeUI, { CustomField, CustomFieldView, render, Image, useProductContext, Fragment } from "@forge/ui";

const View = () => {
    const { platformContext: { fieldValue } } = useProductContext();
    const redLight = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?><!DOCTYPE svg PUBLIC \"-//W3C//DTD SVG 1.1//EN\" \"http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd\"><svg width=\"90px\" height=\"28px\" x=\"0px\" y=\"0px\" version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xml:space=\"preserve\" xmlns:serif=\"http://www.serif.com/\" style=\"fill-rule:evenodd;clip-rule:evenodd;stroke-linejoin:round;stroke-miterlimit:2;\"><path d=\"M90,5.76c0,-2.627 -2.133,-4.76 -4.76,-4.76l-80.48,0c-2.627,0 -4.76,2.133 -4.76,4.76l0,18.48c0,2.627 2.133,4.76 4.76,4.76l80.48,0c2.627,0 4.76,-2.133 4.76,-4.76l0,-18.48Z\" style=\"fill:#dfe1e6;\"/><g id=\"green\"><circle cx=\"16.548\" cy=\"15\" r=\"11.722\" style=\"fill:#b3bac5;\"/><circle cx=\"45.011\" cy=\"15\" r=\"11.722\" style=\"fill:#b3bac5;\"/><circle cx=\"73.452\" cy=\"15\" r=\"11.722\" style=\"fill:#36b37e;\"/></g><g id=\"amber\"><circle cx=\"16.548\" cy=\"15\" r=\"11.722\" style=\"fill:#b3bac5;\"/><circle cx=\"45.011\" cy=\"15\" r=\"11.722\" style=\"fill:#ffab00;\"/><circle cx=\"73.452\" cy=\"15\" r=\"11.722\" style=\"fill:#b3bac5;\"/></g><g id=\"red\"><circle cx=\"16.548\" cy=\"15\" r=\"11.722\" style=\"fill:#ff5630;\"/><circle cx=\"45.011\" cy=\"15\" r=\"11.722\" style=\"fill:#b3bac5;\"/><circle cx=\"73.452\" cy=\"15\" r=\"11.722\" style=\"fill:#b3bac5;\"/></g></svg>";
    const amberLight = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?><!DOCTYPE svg PUBLIC \"-//W3C//DTD SVG 1.1//EN\" \"http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd\"><svg width=\"100%\" height=\"28px\" viewBox=\"0 0 90 30\" x=\"0px\" y=\"0px\" version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xml:space=\"preserve\" xmlns:serif=\"http://www.serif.com/\" style=\"fill-rule:evenodd;clip-rule:evenodd;stroke-linejoin:round;stroke-miterlimit:2;\"><path d=\"M90,5.76c0,-2.627 -2.133,-4.76 -4.76,-4.76l-80.48,0c-2.627,0 -4.76,2.133 -4.76,4.76l0,18.48c0,2.627 2.133,4.76 4.76,4.76l80.48,0c2.627,0 4.76,-2.133 4.76,-4.76l0,-18.48Z\" style=\"fill:#dfe1e6;\"/><g id=\"green\"><circle cx=\"16.548\" cy=\"15\" r=\"11.722\" style=\"fill:#b3bac5;\"/><circle cx=\"45.011\" cy=\"15\" r=\"11.722\" style=\"fill:#b3bac5;\"/><circle cx=\"73.452\" cy=\"15\" r=\"11.722\" style=\"fill:#36b37e;\"/></g><g id=\"amber\"><circle cx=\"16.548\" cy=\"15\" r=\"11.722\" style=\"fill:#b3bac5;\"/><circle cx=\"45.011\" cy=\"15\" r=\"11.722\" style=\"fill:#ffab00;\"/><circle cx=\"73.452\" cy=\"15\" r=\"11.722\" style=\"fill:#b3bac5;\"/></g></svg>";
    const greenLight = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?><!DOCTYPE svg PUBLIC \"-//W3C//DTD SVG 1.1//EN\" \"http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd\"><svg width=\"90px\" height=\"28px\" viewBox=\"0 0 90 30\" version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xml:space=\"preserve\" xmlns:serif=\"http://www.serif.com/\" style=\"fill-rule:evenodd;clip-rule:evenodd;stroke-linejoin:round;stroke-miterlimit:2;\"><path d=\"M90,5.76c0,-2.627 -2.133,-4.76 -4.76,-4.76l-80.48,0c-2.627,0 -4.76,2.133 -4.76,4.76l0,18.48c0,2.627 2.133,4.76 4.76,4.76l80.48,0c2.627,0 4.76,-2.133 4.76,-4.76l0,-18.48Z\" style=\"fill:#dfe1e6;\"/><g id=\"green\"><circle cx=\"16.548\" cy=\"15\" r=\"11.722\" style=\"fill:#b3bac5;\"/><circle cx=\"45.011\" cy=\"15\" r=\"11.722\" style=\"fill:#b3bac5;\"/><circle cx=\"73.452\" cy=\"15\" r=\"11.722\" style=\"fill:#36b37e;\"/></g></svg>";
    let light = "";
    if (fieldValue !== undefined && fieldValue !== null) {
        if (fieldValue.toLowerCase() === "green") {
            light = greenLight;
        } else if (fieldValue.toLowerCase() === "amber") {
            light = amberLight;
        } else if (fieldValue.toLowerCase() === "red") {
            light = redLight;
        }
        return (
            <CustomFieldView>
                <Fragment>
                    <Image
                        alt="Green traffic light"
                        src={`data:image/svg+xml;utf8,${encodeURIComponent(light)}`}
                    ></Image>
                </Fragment>
            </CustomFieldView>
        );
    }
    return (<CustomFieldView></CustomFieldView>);

};

export const run = render(
    <CustomField
        view={<View/>}
    />
);